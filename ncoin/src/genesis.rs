/*!
hashing.rs - Implements the genesis block.

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at <http://mozilla.org/MPL/2.0/>.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/

use crate::structs::{Block, Transaction};
use crate::visualizer::visualize_block;
use crate::miner::create_block;

/// Creates the genesis block for the blockchain.
///
/// This function creates and returns the genesis block, which is the initial block
/// of the blockchain. It includes a predefined transaction that initializes the
/// blockchain with a certain amount of currency.
///
/// If the creation of the genesis block fails due to the absence of a wallet,
/// it prints a warning message and returns `None`.
///
/// # Returns
///
/// Returns `Some(Block)` if the genesis block is successfully created, otherwise `None`.
///
/// # Examples
///
/// ```rust no run
/// let genesis_block = make_genesis_block();
/// ```
pub fn make_genesis_block(blockchain: &[Block]) -> Option<Block> {
    println!("\nMining Genesis block now, this may take some time\n");

    let previous_index: u64 = 0;
    let previous_hash: String = "None".to_string();
    let block_data: Vec<Transaction> = vec![];

    if let Some(genesis_block) = create_block(blockchain, previous_index, previous_hash, block_data) {
        visualize_block(&genesis_block);
        Some(genesis_block)
    } else {
        println!("\nPlease create a wallet then RESTART the application!!! \n
        Failing to make the genesis at boot WILL cause a crash when mining other blocks!!!\n");
        None
    }
}
