/*!
mining.rs - Implements the mining system.

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at <http://mozilla.org/MPL/2.0/>.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/

use crate::structs::{Block, BlockData, Transaction};
use crate::hashing::get_block_hash;
use crate::transaction::is_transaction_valid;
use crate::wallet::{get_keys, get_wallet_name};

use std::time::{SystemTime, UNIX_EPOCH, Duration};
use std::sync::{Arc, Mutex};
use std::thread;
use std::sync::atomic::{AtomicBool, Ordering};

use hex::{decode, encode};

/// The difficulty level of the mining process.
///
/// This constant determines the number of leading zeroes required in the
/// SHA-256 hash of a block for it to be considered valid.
static DIFFICULTY: u64 = 12;

/// The reward you get for mining a block
pub static BLOCK_REWARD: u64 = 25000000;

/// Creates a new block for the blockchain.
///
/// This function creates a new block with the given parameters. It initiates
/// multiple threads to concurrently mine the block until a valid nonce is found.
///
/// # Arguments
///
/// * `previous_index` - The index of the previous block in the blockchain.
/// * `previous_hash` - The hash of the previous block in the blockchain.
/// * `block_data_data` - The serialized transaction data to be included in the new block.
///
/// # Returns
///
/// `Some(Block)` - A `Block` struct representing the newly created block if successful.
/// `None` - If mining fails to find a valid nonce within acceptable parameters.
///
/// # Examples
///
/// ```rust no run
/// use crate::blockchain::{create_block, Block};
///
/// let previous_index = 0;
/// let previous_hash = String::from("previous_hash");
/// let block_data_data = vec![vec![0u8; 32], vec![1u8; 32]]; // Example serialized transaction data
///
/// let block = create_block(previous_index, previous_hash, block_data_data);
/// ```
pub fn create_block(blockchain: &[Block], previous_index: u64, previous_hash: String, transaction_data: Vec<Transaction>) -> Option<Block> {
    let thread_count: usize = match thread::available_parallelism() {
        Ok(n) => n.get() / 2,
        Err(_) => 1,
    };

    let index: u64 = previous_index + 1;

    let sys_time: SystemTime = SystemTime::now();
    let since_the_epoch: Duration = sys_time.duration_since(UNIX_EPOCH).expect("Time went backwards");
    let unix_timestamp: u64 = since_the_epoch.as_secs();

    let block_data: BlockData;
    if let Some(block_data_attempt) = mine_block(blockchain, index, unix_timestamp, previous_hash, transaction_data, thread_count.try_into().unwrap_or(1)) {
        block_data = block_data_attempt;
    } else {return None;}

    let block_hash: String = get_block_hash(&block_data);
    let block: Block = Block {
        block_hash,
        block_data,
    };

    Some(block)
}

/// Mines a block with a valid nonce.
///
/// This function attempts to find a valid nonce for the block by incrementing
/// the nonce in parallel across multiple threads until a valid nonce is found.
///
/// # Arguments
///
/// * `index` - The index of the block in the blockchain.
/// * `unix_timestamp` - The Unix timestamp of when the block is created.
/// * `previous_hash` - The hash of the previous block in the blockchain.
/// * `block_data` - The data to be included in the block.
/// * `thread_count` - The number of threads to use for mining.
///
/// # Returns
///
/// An `Option<BlockData>` containing the mined block data with a valid nonce,
/// or `None` if mining fails (e.g., if wallet setup fails or no valid nonce is found).
///
/// # Example
///
/// ```rust no run
/// # use crate::miner::mine_block;
/// # fn main() {
/// let index = 1;
/// let unix_timestamp = 1623079393;
/// let previous_hash = "0000abcdef...".to_string();
/// let block_data = vec![vec![0, 1, 2], vec![3, 4, 5]];
/// let thread_count = 4;
///
/// if let Some(block_data) = mine_block(index, unix_timestamp, previous_hash, block_data, thread_count) {
///     println!("Mined block with nonce: {}", block_data.nonce);
/// } else {
///     println!("Failed to mine block!");
/// }
/// # }
/// ```
fn mine_block(blockchain: &[Block], index: u64, unix_timestamp: u64, previous_hash: String, transaction_data: Vec<Transaction>, thread_count: u64) -> Option<BlockData> {
    let transaction: Vec<Transaction> = validate_block_data(blockchain, transaction_data);

    let found_valid_nonce: Arc<AtomicBool> = Arc::new(AtomicBool::new(false));
    let nonce: Arc<Mutex<u64>> = Arc::new(Mutex::new(0));
    let miner_address: String;
    let name: String = get_wallet_name()?;
    if let Ok((_, public_key)) = get_keys(&name) { miner_address = encode(public_key.serialize()); }
    else { println!("\nPlease use the \"wallet\" command to create a wallet before mining a block!"); return None; }

    let mut threads: Vec<thread::JoinHandle<()>> = vec![];

    for thread_id in 0..thread_count {
        let found_valid_nonce: Arc<AtomicBool> = Arc::clone(&found_valid_nonce);
        let nonce: Arc<Mutex<u64>> = Arc::clone(&nonce);

        let previous_hash_clone: String = previous_hash.clone();
        let transaction_data_clone: Vec<Transaction> = transaction.clone();
        let miner_address_clone: String = miner_address.clone();

        let thread_handle: thread::JoinHandle<()> = thread::spawn(move || {
            let mut test_nonce_value: u64 = thread_id;

            while !found_valid_nonce.load(Ordering::Relaxed) {
                let block_data_attempt: BlockData = BlockData {
                    miner_address: miner_address_clone.clone(),
                    index,
                    unix_timestamp,
                    nonce: test_nonce_value,
                    previous_hash: previous_hash_clone.clone(),
                    transactions: transaction_data_clone.clone(),
                };

                let block_hash_attempt: String = get_block_hash(&block_data_attempt);
                if is_nonce_valid(block_hash_attempt) {
                    *nonce.lock().unwrap() = test_nonce_value;
                    found_valid_nonce.store(true, Ordering::Relaxed);
                    break;
                } else { test_nonce_value += thread_count; }
            }
        });
        
        threads.push(thread_handle);
    }

    for thread in threads {
        thread.join().unwrap();
    }

    let nonce_value: u64 = *nonce.lock().unwrap();
    let final_data: BlockData = BlockData {
        miner_address,
        index,
        unix_timestamp,
        nonce: nonce_value,
        previous_hash,
        transactions: transaction
    };

    Some(final_data)
}

/// Validates a list of block data transactions.
///
/// This function iterates through a list of serialized transaction data (`test_block_data`),
/// deserializes each transaction, and validates its integrity using the `is_transaction_valid`
/// function. Valid transactions are collected into a new vector (`valid_block_data`), while
/// invalid transactions are logged to the console.
///
/// # Arguments
///
/// * `test_block_data` - A vector of vectors containing serialized transaction data.
///
/// # Returns
///
/// A vector of valid serialized transaction data (`Vec<Vec<u8>>`).
///
/// # Examples
///
/// ```rust no run
/// use crate::transaction::{deserialize_transaction, Transaction};
/// use crate::validation::validate_block_data;
///
/// let transaction_data: Vec<Vec<u8>> = vec![
///     vec![0, 1, 2, 3],
///     vec![4, 5, 6, 7],
///     vec![8, 9, 10, 11],
/// ];
///
/// let valid_transactions = validate_block_data(transaction_data);
/// assert_eq!(valid_transactions.len(), 3); // Assuming all transactions are valid
/// ```
fn validate_block_data(blockchain: &[Block], test_transaction_data: Vec<Transaction>) -> Vec<Transaction> {
    let mut valid_block_data: Vec<Transaction> = vec![];
    let mut transaction_index: u64 = 0;
    for transaction_test in test_transaction_data {
        if is_transaction_valid(blockchain, &transaction_test) {
            valid_block_data.push(transaction_test);
            transaction_index += 1;
        } else {
            println!("Transaction {} not valid", transaction_index);
            transaction_index += 1;
        }
    }
    valid_block_data
}

/// Checks if a given SHA-256 hash satisfies the mining difficulty.
///
/// This function checks if the given SHA-256 hash has a sufficient number of
/// leading zeroes according to the current mining difficulty.
///
/// # Arguments
///
/// * `block_hash_attempt` - The SHA-256 hash to check.
///
/// # Returns
///
/// `true` if the hash satisfies the mining difficulty; otherwise, `false`.
/// 
/// # Panics
///
/// This function may panic if the provided `block_hash_attempt` cannot be decoded
/// into bytes. This should only happen in exceptional circumstances and indicates
/// a severe issue with the integrity of the hash.
fn is_nonce_valid(block_hash_attempt: String) -> bool {
    let sha256_hash: Vec<u8> = decode(block_hash_attempt).expect("If you got the error about a lack of a genesis block, this is an EXPECTED crash and that is why.");

    if DIFFICULTY == 0 {
        return true;
    }

    let byte_index: usize = (DIFFICULTY / 8) as usize;
    let bit_index: usize = (DIFFICULTY % 8) as usize;

    for i in 0..byte_index {
        if sha256_hash.as_slice()[i] != 0 { return false; }
    }

    if bit_index > 0 {
        let mask: u8 = 0xFF << (8 - bit_index);
        if sha256_hash.as_slice()[byte_index] & mask != 0 { return false; }
    }

    true
}
