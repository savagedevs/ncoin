/*!
transaction.rs - Implements the transaction system.

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at <http://mozilla.org/MPL/2.0/>.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/

use crate::hashing::get_trans_hash;
use crate::structs::{Transaction, Block};
use crate::wallet::{does_wallet_exist, get_keys, get_wallet_ballance, get_wallet_name};

use secp256k1::PublicKey;
use secp256k1::{Secp256k1, Message, ecdsa::Signature};
use hex::{decode, encode};

use std::io::stdin;
use std::error::Error;

const FEE_PERCENT: f64 = 0.5;

impl Transaction {
    pub fn new(sender_pub_key: String, receiver_pub_key: String, amount_in_nanos: u64, burn_fee: u64, last_block_hash: String, public_memo: String, transaction_signature: String) -> Self {
        Transaction {
            sender_pub_key,
            receiver_pub_key,
            amount_in_nanos,
            burn_fee,
            last_block_hash,
            public_memo,
            transaction_signature,
        }
    }
}

impl Default for Transaction {
    fn default() -> Self {
        Self::new("Invalid".to_string(), "Invalid".to_string(), 0, 0, "Invalid".to_string(), "".to_string(), "Invalid".to_string())
    }
}

/// Serializes a `Transaction` struct into binary data.
///
/// # Arguments
///
/// * `transaction` - The transaction to be serialized.
///
/// # Returns
///
/// A `Vec<u8>` containing the serialized transaction data.
///
/// # Example
///
/// ```rust no run
/// let transaction_data = serialize_transaction(&transaction);
/// ```
pub fn serialize_transaction(transaction: &Transaction) -> Vec<u8> {
    bincode::serialize(transaction).unwrap_or_default()
}

/// Creates a new transaction with user input.
///
/// This function guides the user through creating a new transaction,
/// prompting for necessary details like receiver's wallet address,
/// amount, and public memo. It then signs the transaction using the
/// sender's private key and returns the completed `Transaction` struct.
///
/// # Arguments
///
/// * `last_block_hash` - The hash of the last block in the blockchain.
///
/// # Returns
///
/// A `Result` containing the created `Transaction` on success, or a boxed
/// error on failure, indicating why the transaction creation failed.
///
/// # Errors
///
/// This function may fail if:
/// - The user does not have a wallet.
/// - Input reading fails.
/// - Decoding transaction hash fails.
/// - Signing the transaction fails.
///
/// # Example
///
/// ```rust no run
/// let last_block_hash = "previous_block_hash".to_string();
/// let transaction_result = create_transaction(last_block_hash);
/// ```
pub fn create_transaction(blockchain: &[Block], last_block_hash: String) -> Result<Transaction, Box<dyn Error>> {
    let name: String = get_wallet_name().ok_or(std::io::Error::new(std::io::ErrorKind::InvalidInput, "Failed to get wallet name"))?;
    if !does_wallet_exist(&name)? {
        println!("\nYou do not have a wallet, to make one type \"wallet\" or \"w\" in the CLI\n");
        return Err(Box::new(std::io::Error::new(std::io::ErrorKind::InvalidInput, "Must have a wallet to make transactions")));
    }
    let (sender_secret_key, sender_public_key) = get_keys(&name)?;
    let sender_pub_key: String = encode(sender_public_key.serialize());

    println!("Receiver Wallet: ");
    let mut receiver_pub_key: String = String::new();
    stdin().read_line(&mut receiver_pub_key).unwrap_or_default();
    receiver_pub_key = receiver_pub_key.trim().to_string();

    let amount_in_nanos: u64;
    loop {
        println!("Amount in Nanos (one nano is 0.0000001 of an NCoin): ");
        let mut input: String = String::new();
        stdin().read_line(&mut input).expect("Failed to read line");

        match input.trim().parse::<u64>() {
            Ok(num) => { amount_in_nanos = num; break; },
            Err(_) => { println!("Invalid input. Please enter a valid u64 number."); continue; }
        }
    }

    let burn_fee: u64 = (amount_in_nanos as f64 * (FEE_PERCENT / 100.0)) as u64 + 500;

    println!("Public Memo: ");
    let mut public_memo: String = String::new();
    stdin().read_line(&mut public_memo).unwrap_or_default();
    public_memo = public_memo.trim().to_string();

    let secp: Secp256k1<secp256k1::All> = Secp256k1::new();
    let transaction_hash: String = get_trans_hash(sender_pub_key.clone(), receiver_pub_key.clone(), amount_in_nanos, burn_fee, last_block_hash.clone(), public_memo.clone());
    let message: Message = Message::from_digest_slice(&decode(transaction_hash)?)?;
    let signature: Signature = secp.sign_ecdsa(&message, &sender_secret_key);
    let transaction_signature: String = encode(signature.serialize_compact());

    let transaction: Transaction = Transaction::new(sender_pub_key, receiver_pub_key, amount_in_nanos, burn_fee, last_block_hash, public_memo, transaction_signature);

    if !is_transaction_valid(blockchain, &transaction) {
        println!("\nThe transaction is not valid, ERROR!!! \n\
        You may be trying to send more then you have, check with \"b\", or you have an invalid wallet.\n\
        Do not try to force it some other way as the miners may reject your transaction\n");
        return Err(Box::new(std::io::Error::new(std::io::ErrorKind::InvalidInput, "Transaction invalid")));
    }

    Ok(transaction)
}

/// Checks the validity of a transaction.
///
/// This function verifies the validity of a given transaction by checking its cryptographic
/// signature against the sender's public key. It performs the following checks:
///
/// 1. Decodes the transaction hash.
/// 2. Converts the transaction hash into a cryptographic message digest.
/// 3. Decodes the transaction signature.
/// 4. Decodes the sender's public key.
/// 5. Verifies the cryptographic signature against the sender's public key
/// 6. Verifies the sender has enough nanos for the transaction
///
/// # Arguments
///
/// * `test_transaction` - A `Transaction` struct to be validated.
///
/// # Returns
///
/// Returns `true` if the transaction is valid and the signature verification passes,
/// otherwise returns `false`.
///
/// # Examples
///
/// ```rust no run
/// use crate::transaction::Transaction;
/// use crate::validation::is_transaction_valid;
///
/// let transaction = Transaction::new(
///     "sender_public_key".to_string(),
///     "receiver_public_key".to_string(),
///     100000000,
///     10,
///     "last_block_hash".to_string(),
///     "Memo".to_string(),
///     "transaction_signature".to_string(),
/// );
///
/// assert!(is_transaction_valid(transaction));
/// ```
pub fn is_transaction_valid(blockchain: &[Block], test_transaction: &Transaction) -> bool {
    let secp: Secp256k1<secp256k1::All> = Secp256k1::new();

    let sender_pub_key: String = test_transaction.sender_pub_key.clone();
    let receiver_pub_key: String = test_transaction.receiver_pub_key.clone();
    let amount_in_nanos: u64 = test_transaction.amount_in_nanos;
    let burn_fee: u64 = test_transaction.burn_fee;
    let last_block_hash: String = test_transaction.last_block_hash.clone();
    let public_memo: String = test_transaction.public_memo.clone();

    let transaction_hash: String = get_trans_hash(sender_pub_key.clone(), receiver_pub_key, amount_in_nanos, burn_fee, last_block_hash, public_memo);
    let digest: Vec<u8>;
    if let Ok(transaction_hash_test) = decode(transaction_hash) {
        digest = transaction_hash_test
    } else { return false; }

    let message: Message;
    if let Ok(message_test) = Message::from_digest_slice(&digest) {
        message = message_test
    } else { return false; }

    let signature: Signature;
    if let Ok(signature_test) = decode(test_transaction.transaction_signature.clone()) {
        if let Ok(signature_uncompact) = Signature::from_compact(&signature_test) {
            signature = signature_uncompact
        } else { return false; }
    } else { return false; }

    let public_key: PublicKey;
    if let Ok(public_key_test) = decode(sender_pub_key.clone()) {
        if let Ok(public_key_uncompact) = PublicKey::from_slice(&public_key_test) {
            public_key = public_key_uncompact
        } else { return false; }
    } else { return false; }

    let has_enough: bool;
    if let Some(ballance_test) = get_wallet_ballance(&blockchain.to_vec(), sender_pub_key) {
        has_enough = ballance_test >= (amount_in_nanos + burn_fee);
    } else { has_enough = false }

    has_enough && secp.verify_ecdsa(&message, &signature, &public_key).is_ok()
}
