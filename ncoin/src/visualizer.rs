/*!
visualizer.rs - Implements the block viewing system.

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at <http://mozilla.org/MPL/2.0/>.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/

use crate::structs::{Block, BlockData, Transaction};

/// Visualizes the content of a block.
///
/// This function prints out detailed information about the given block,
/// including block hash, previous block hash, block index, timestamp,
/// nonce, and details of each transaction within the block.
///
/// # Arguments
///
/// * `block` - A reference to the `Block` struct to visualize.
pub fn visualize_block(block: &Block) {
    let block_data: &BlockData = &block.block_data;
    let transactions: &Vec<Transaction> = &block_data.transactions;

    println!("Block Hash: \t\t{}", block.block_hash);
    println!("\tBlock Miner: \t\t{}", block_data.miner_address);
    println!("\tPrevious Block Hash: \t{}", block_data.previous_hash);
    println!("\tBlock Index: \t\t{}", block_data.index);
    println!("\tBlock Timestamp: \t{}", block_data.unix_timestamp);
    println!("\tBlock Nonce: \t\t{}", block_data.nonce);

    let mut transaction_pos: u64 = 1;
    for transaction in transactions {
        println!("\tTransaction {} data: ", transaction_pos);
        println!("\t\tSender Public Key: \t{}", transaction.sender_pub_key);
        println!("\t\tReciever Public Key: \t{}", transaction.receiver_pub_key);
        println!("\t\tAmount in nanos: \t{}", transaction.amount_in_nanos);
        println!("\t\tBurn fee: \t\t{}", transaction.burn_fee);
        println!("\t\tLast Block Hash: \t{}", transaction.last_block_hash);
        println!("\t\tPublic Memo: \t\t{}", transaction.public_memo);
        println!("\t\tSignature: \t\t{}", transaction.transaction_signature);
        transaction_pos += 1;
    }

    println!();
}
