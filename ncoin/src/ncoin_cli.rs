/*!
ncoin_cli.rs - Implements the CLI for NCoin

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at <http://mozilla.org/MPL/2.0/>.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/

use crate::structs::{Block, Transaction};
use crate::miner::create_block;
use crate::transaction::create_transaction;
use crate::visualizer::visualize_block;
use crate::wallet::{view_ballance, wallet_setup, get_wallet_name};

use std::io::{stdout, stdin, Write};

/// Starts the NCoin Command Line Interface (CLI) loop.
///
/// This function initializes the CLI loop where users can interact with NCoin
/// by entering commands via the command line. The CLI supports commands for
/// mining blocks, sending transactions, inspecting the blockchain, setting up
/// a wallet, and exiting the program.
///
/// # Arguments
///
/// * `blockchain` - A mutable reference to the blockchain.
pub fn cli_loop(blockchain: &mut Vec<Block>) {
    println!("NCoin CLI - v0.6.0\n\
    \n\
    Maintainer(s): Daniel McLarty <daniel@savagedves.com>\n\
    This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.\n\
    (C) 2024 Daniel McLarty\n");

    let mut input: String = String::new();
    let mut block_data: Vec<Transaction> = vec![];

    loop {
        print!("ncoin_cli> ");
        stdout().flush().unwrap();

        input.clear();
        stdin().read_line(&mut input).unwrap_or_default();

        if parse_input(&input, blockchain, &mut block_data) { break; }
    }
}

/// Parses user input and executes corresponding actions.
///
/// This function parses the user input provided via the command line
/// and executes the appropriate action based on the input command.
/// Supported commands include mining blocks, sending transactions,
/// inspecting the blockchain, setting up a wallet, and exiting the program.
///
/// # Arguments
///
/// * `input` - The user input string.
/// * `blockchain` - A mutable reference to the blockchain.
/// * `block_data` - A mutable reference to the block data vector.
///
/// # Returns
///
/// `true` if the user wants to exit the program; otherwise, `false`.
fn parse_input(input: &str, blockchain: &mut Vec<Block>, block_data: &mut Vec<Transaction>) -> bool {
    let trimmed_input: &str = input.trim();
    let previous_block: &Block = blockchain.last().expect("There REALLY should be a genesis before this so this should NEVER show");
    let previous_index: u64 = previous_block.block_data.index;
    let previous_hash: String = previous_block.block_hash.clone();

    match trimmed_input {
        "exit" | "q" => true,
        "mine" | "m" => {
            let block: Block;
            let single_block_data: Vec<Transaction> = block_data.clone();
            if let Some(block_attempt) = create_block(blockchain, previous_index, previous_hash, single_block_data) { block = block_attempt }
            else { println!("Failed to create block!!!"); return false; }
            blockchain.push(block); 
            block_data.clear();
            false
        }
        "send" | "s" => {
            let transaction: Transaction;
            if let Ok(trans_attempt) = create_transaction(blockchain, previous_hash) { transaction = trans_attempt }
            else { println!("Failed to create transaction"); return false; }
            block_data.push(transaction);
            false
        }
        "inspect" | "i" => {
            for block in blockchain { visualize_block(block); }
            false
        }
        "wallet" | "w" => {
            match wallet_setup() { Ok(_) => (), Err(_) => println!("Failed to make wallet") };
            false
        }
        "ballance" | "b" => {
            let name: String;
            if let Some(name_test) = get_wallet_name() {
                name = name_test
            } else { println!("That wallet does not exist, ERROR!!!"); return false; }
            view_ballance(blockchain, &name);
            false
        }
        "help" | "h" => { 
            println!("\"help\" \"h\": \tPrint this message"); 
            println!("\"mine\" \"m\": \tMine a block");
            println!("\"send\" \"s\": \tSend a transaction");
            println!("\"inspect\" \"i\" \tInspect the blockchain");
            println!("\"wallet\" \"w\" \tSetup a wallet");
            println!("\"ballance\" \"b\" \tSee your ballance");
            println!("\"exit\" \"q\": \tExit the program");
            false
        }
        _ => { println!("Unkown command"); false }
    }
}
