/*!
save_load.rs - Implements the saving and loading systems

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at <http://mozilla.org/MPL/2.0/>.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/

use crate::structs::Block;

use serde_json;
use home::home_dir;

use std::fs::{create_dir_all, read_dir, DirEntry, File, Metadata, OpenOptions};
use std::io::{Write, Read};
use std::path::{Path, PathBuf};
use std::error::Error;

/// Saves the blockchain to disk as individual block files.
///
/// This function serializes each block in the blockchain to a JSON string
/// and saves it as an individual block file in the directory specified by `blockchain_path()`.
///
/// # Arguments
///
/// * `blockchain` - A reference to the vector of `Block` structs representing the blockchain.
///
/// # Returns
///
/// `Result<(), Box<dyn Error>>` - A result indicating success (`Ok`) or an error (`Err`) if there
/// are issues creating the block files or writing to them.
///
/// # Errors
///
/// This function can return an error if:
/// - There is an issue creating the directory for the blockchain files.
/// - There is an issue creating or writing to any of the block files.
///
/// # Examples
///
/// ```rust no run
/// # use crate::save_load::save_blockchain;
/// let blockchain = vec![/* Populate with your blockchain data */];
/// match save_blockchain(&blockchain) {
///     Ok(_) => println!("Blockchain saved successfully."),
///     Err(err) => eprintln!("Error saving blockchain: {}", err),
/// }
/// ```
pub fn save_blockchain(blockchain: &Vec<Block>) -> Result<(), Box<dyn Error>> {
    for block in blockchain {
        let json_string: String = serde_json::to_string(block).unwrap();
        let filename: String = "Block_".to_string() + block.block_data.index.to_string().as_str() + ".block";
        create_dir_all(blockchain_path()?)?;
        let mut block_file: File = File::create(blockchain_path()? + filename.as_str())?;
        block_file.write_all(json_string.as_bytes())?;
    }
    Ok(println!("Blockchain saved"))
}

pub fn load_blockchain() -> Option<Vec<Block>>{
    let blockchain_path: String = blockchain_path().ok()?;
    let mut blocks: Vec<DirEntry> = read_dir(&blockchain_path).ok()?
        .filter_map(Result::ok)
        .collect();
    blocks.sort_by_key(|entry| entry.file_name());
    let mut last_index: u64 = 0;
    let mut valid_blocks: Vec<Block> = vec![];

    for block_dir in blocks {
        let blockchain_path_clone: String = blockchain_path.clone();
        if let Some(block_test) = load_block(block_dir, blockchain_path_clone) {
            let index: u64 = block_test.block_data.index;
            if index != last_index + 1 { println!("Missing last block or invalid index at block index {}, ERROR !!!", index); return None; }
            valid_blocks.push(block_test);
            last_index = index
        } else { continue; }
    }
    if valid_blocks.is_empty() { println!("Saved blockchain not found"); return None; }
    Some(valid_blocks)
}

fn load_block(block_dir: DirEntry, blockchain_path: String) -> Option<Block>{
    let metadata: Metadata = block_dir.metadata().ok()?;
    let block: Block;

    if metadata.is_file() {
        let file_name: String = block_dir.file_name().to_str()?.to_string();
        let full_path: String = blockchain_path + file_name.as_str();
        if Path::new(&full_path).extension()? != "block" { return None; }
        let mut block_file: File = OpenOptions::new().read(true).open(full_path).ok()?;
        let mut json_block_string: String = String::new();
        block_file.read_to_string(&mut json_block_string).ok()?;
        block= serde_json::from_str::<Block>(&json_block_string).ok()?;
        Some(block)
    } else { None }
}

/// Constructs the path where block files are stored.
///
/// This function constructs the directory path `.config/ncoin/keys/` in the user's
/// home directory. It returns the path as a `String` if successful.
///
/// # Errors
///
/// Returns an error if the home directory cannot be retrieved or converted to a valid
/// string representation.
///
/// # Examples
///
/// ```rust no run
/// # use crate::save_load::blockchain_path;
/// # fn main() {
/// let path = blockchain_path().unwrap_or_else(|err| {
///     panic!("Failed to get blockchain path: {}", err);
/// });
/// println!("Blockchain files are stored at: {}", path);
/// # }
/// ```
fn blockchain_path() -> Result<String, Box<dyn Error>> {
    let mut path_root: PathBuf = home_dir().ok_or("Value is None")?;
    path_root.push(".config/ncoin/blockchain/");

    Ok(path_root.to_str().ok_or("Value is None")?.to_string())
}
