/*!
wallet.rs - Implements the wallet system

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at <http://mozilla.org/MPL/2.0/>.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/

use crate::structs::{KeyPair, Block};
use crate::miner::BLOCK_REWARD;

use secp256k1::{Secp256k1, SecretKey, PublicKey};
use rand::rngs::OsRng;
use rand::RngCore;
use hex::{encode, decode};
use home::home_dir;

use std::fs::{create_dir_all, read_dir, File, Metadata, OpenOptions, ReadDir};
use std::io::{stdout, stdin, Write, Read};
use std::path::{Path, PathBuf};
use std::error::Error;

/// Sets up a new wallet by generating and storing key pairs.
///
/// This function creates a new wallet by generating a pair of secret and public keys
/// using the secp256k1 elliptic curve cryptography algorithm. It stores the keys in
/// the user's home directory under `.config/ncoin/keys/` and verifies their correctness
/// by comparing them with retrieved keys.
///
/// # Errors
///
/// This function returns an error if there is any issue during key generation, storage,
/// or verification.
///
/// # Examples
///
/// ```rust no run
/// # use crate::wallet::wallet_setup;
/// # fn main() {
/// if let Err(e) = wallet_setup() {
///     println!("Error setting up wallet: {}", e);
/// }
/// # }
/// ```
pub fn wallet_setup() -> Result<(), Box<dyn Error>> {
    let name: String = get_wallet_name().ok_or(std::io::Error::new(std::io::ErrorKind::InvalidInput, "Failed to get wallet name"))?;

    let (secret_key, public_key) = create_key_pair(&name)?;
    let keypair: KeyPair = KeyPair {
        secret_key: secret_key[..].to_vec(),
        public_key: public_key.serialize().to_vec(),
    };

    store_keys(&keypair, &name)?;

    let (decoded_secret_key, decoded_public_key) = get_keys(&name)?;

    if (secret_key == decoded_secret_key) && (public_key == decoded_public_key) {
        println!("Wallet successfully, created, stored, and tested");
    }
    else {
        println!("Stored wallet does not match generated wallet, !!! CRITICAL ERROR !!!");
        return Err(Box::new(std::io::Error::new(std::io::ErrorKind::InvalidInput, "Failed to make wallet")));
    }

    Ok(())
}


/// Retrieves the secret and public keys from stored files.
///
/// This function reads and decodes the secret and public keys from files stored
/// in the user's home directory under `.config/ncoin/keys/`. It returns the keys
/// as `SecretKey` and `PublicKey` types.
///
/// # Errors
///
/// This function returns an error if it fails to read or decode the keys from the files,
/// or if the wallet files do not exist or are corrupted.
///
/// # Examples
///
/// ```rust no run
/// # use crate::wallet::get_keys;
/// # fn main() {
/// match get_keys() {
///     Ok((secret_key, public_key)) => {
///         println!("Successfully retrieved keys");
///     },
///     Err(e) => println!("Error retrieving keys: {}", e),
/// }
/// # }
/// ```
pub fn get_keys(name: &String) -> Result<(SecretKey, PublicKey), Box<dyn Error>> {
    if !does_wallet_exist(name)? {
        println!("\nError, wallet not found or corrupted, making a new one now");
        wallet_setup()?;
    }

    let mut private_key_file: File = OpenOptions::new().read(true).open(key_path()? + name)?;
    let mut encoded_secret_key: String = String::new();
    private_key_file.read_to_string(&mut encoded_secret_key)?;

    let mut public_key_file: File = OpenOptions::new().read(true).open(key_path()? + name + ".pub")?;
    let mut encoded_public_key: String = String::new();
    public_key_file.read_to_string(&mut encoded_public_key)?;

    let decoded_secret_key_bytes: Vec<u8> = decode(encoded_secret_key)?;
    let decoded_public_key_bytes: Vec<u8> = decode(encoded_public_key)?;

    let decoded_secret_key: SecretKey = SecretKey::from_slice(&decoded_secret_key_bytes)?;
    let decoded_public_key: PublicKey = PublicKey::from_slice(&decoded_public_key_bytes)?;
    
    Ok((decoded_secret_key, decoded_public_key))
}

/// Checks if wallet files exist in the expected directory.
///
/// This function checks if the wallet files `wallet` and `wallet.pub` exist
/// in the directory `.config/ncoin/keys/` in the user's home directory.
///
/// # Errors
///
/// This function returns an error if it fails to retrieve the home directory
/// or encounters an I/O error while checking for the existence of wallet files.
///
/// # Examples
///
/// ```rust no run
/// # use crate::wallet::does_wallet_exist;
/// # fn main() {
/// match does_wallet_exist() {
///     Ok(true) => println!("Wallet files exist"),
///     Ok(false) => println!("Wallet files do not exist"),
///     Err(e) => println!("Error checking wallet existence: {}", e),
/// }
/// # }
/// ```
pub fn does_wallet_exist(name: &String) -> Result<bool, Box<dyn Error>> {
    Ok(Path::new(&(key_path()? + name)).exists() &&
    Path::new(&(key_path()? + name + ".pub")).exists())
}

/// Calculates the wallet balance based on transactions in the blockchain.
///
/// This function iterates through the blockchain and calculates the balance of a specified
/// wallet address (`wallet_address`). It sums up the amounts received, subtracts the amounts
/// sent from the wallet, and adds the blcok reward if the wallet mined the block.
/// The result is returned as an `Option<u64>`, where `Some(balance)` represents a valid balance, 
/// and `None` indicates an overflow occurred during subtraction.
///
/// # Arguments
///
/// * `blockchain` - A reference to a vector of `Block` structs representing the blockchain.
/// * `wallet_address` - A `String` containing the public key (wallet address) of the wallet
///                      whose balance is to be calculated.
///
/// # Returns
///
/// An `Option<u64>` representing the balance in nanos of the specified wallet address.
///
/// # Examples
///
/// ```rust no run
/// use crate::blockchain::Block;
/// use crate::transaction::{deserialize_transaction, Transaction};
/// use crate::wallet::get_wallet_balance;
///
/// let blockchain: Vec<Block> = vec![
///     // Insert blockchain blocks here
/// ];
///
/// let wallet_address = "abcd1234".to_string(); // Replace with actual wallet address
/// let balance = get_wallet_balance(&blockchain, wallet_address);
///
/// match balance {
///     Some(amount) => println!("Wallet balance: {} nanos", amount),
///     None => println!("Overflow occurred while calculating balance"),
/// }
/// ```
pub fn get_wallet_ballance(blockchain: &Vec<Block>, wallet_address: String) -> Option<u64> {
    let mut ballance_in_nanos: u64 = 0;
    for block in blockchain {
        for transaction in &block.block_data.transactions {
            let sender_wallet: String = transaction.sender_pub_key.clone();
            let receiver_wallet: String = transaction.receiver_pub_key.clone();

            if wallet_address == receiver_wallet { ballance_in_nanos += transaction.amount_in_nanos }
            else if wallet_address == sender_wallet { ballance_in_nanos = ballance_in_nanos.checked_sub(transaction.amount_in_nanos + transaction.burn_fee)? }
        }

        if wallet_address == block.block_data.miner_address { ballance_in_nanos += BLOCK_REWARD }
    }

    Some(ballance_in_nanos)
}

/// Displays the balance of the wallet associated with the current user.
///
/// This function checks for the existence of the wallet, retrieves the public key,
/// calculates the balance in nanos using the blockchain, and then converts the balance
/// to NCoins for display.
///
/// If the wallet does not exist, it prompts the user to create one. If any errors occur
/// during wallet existence check, public key retrieval, balance calculation, or if the
/// balance is negative (which shouldn't happen), an error message is printed.
///
/// # Arguments
///
/// * `blockchain` - A reference to a vector of `Block` structs representing the blockchain.
///
/// # Examples
///
/// ```rust no run
/// use crate::blockchain::Block;
/// use crate::wallet::view_balance;
///
/// let blockchain: Vec<Block> = vec![
///     // Insert blockchain blocks here
/// ];
///
/// view_balance(&blockchain);
/// ```
pub fn view_ballance(blockchain: &Vec<Block>, name: &String) {
    let wallet_existance: bool;
    if let Ok(wallet_existance_test) = does_wallet_exist(name) {
        wallet_existance = wallet_existance_test
    } else { println!("\nFailed to test for wallet existance, ERROR!!!\n"); return; }
    if !wallet_existance {
        println!("You do not have a wallet, to make one type \"wallet\" or \"w\" in the CLI\n");
        return;
    }

    let public_key: PublicKey;
    if let Ok((_, public_key_test)) = get_keys(name) {
        public_key = public_key_test
    } else { println!("\nFailed to get your public key, ERROR!!!\n"); return; }
    let wallet_address: String = encode(public_key.serialize());

    let ballance_in_nanos: u64;
    if let Some(ballance_test) = get_wallet_ballance(blockchain, wallet_address) {
        ballance_in_nanos = ballance_test
    } else { println!("\nFailed to get ballance or ballance is negative, ERROR!!!\n"); return; }

    let ballance_in_ncoins: f64 = ballance_in_nanos as f64 / 1000000.0;
    println!("Your ballance (in nanos) is {}", ballance_in_nanos);
    println!("That is {} NCoins\n", ballance_in_ncoins)
}

/// Retrieves the name of a wallet from the user input.let wallet_path_clone: String = wallet_path.clone();
///
/// This function lists all existing wallet files in the user's wallet directory,
/// prompts the user to enter a wallet name, and validates the input against invalid
/// characters. If the input is empty or contains invalid characters, it prompts the user
/// again until a valid wallet name is entered.
///
/// # Returns
///
/// An `Option<String>` containing the validated wallet name entered by the user.
///
/// # Examples
///
/// ```rust no run
/// use crate::wallet::get_wallet_name;
///
/// let wallet_name = get_wallet_name();
/// match wallet_name {
///     Some(name) => println!("Selected wallet name: {}", name),
///     None => println!("No valid wallet name entered"),
/// }
/// ```
pub fn get_wallet_name() -> Option<String> {
    println!("These are your current wallet(s):");

    let wallet_path: String = key_path().ok()?;
    let wallets: ReadDir = read_dir(wallet_path.clone()).ok()?;

    for wallet in wallets.flatten() {
        let wallet_path_clone: String = wallet_path.clone();
        let metadata: Metadata;
        if let Ok(metadata_test) = wallet.metadata() {
            metadata = metadata_test
        } else { continue; }

        if metadata.is_file() {
            let file_name: String = wallet.file_name().to_str()?.to_string();
            let full_path: String = wallet_path_clone + file_name.as_str();
            if Path::new(&full_path).extension().is_none() { println!("{}", file_name); }
        }
    }

    loop {
        print!("\nEnter a wallet name: ");
        stdout().flush().unwrap();

        let mut input: String = String::new();
        stdin().read_line(&mut input).unwrap();

        let wallet_name: &str = input.trim();

        if wallet_name.is_empty() ||  contains_invalid_chars(wallet_name) {
            println!("Wallet name is invalid");
            continue;
        }

        println!();
        return Some(wallet_name.to_string());
    }
}

/// Constructs the path where wallet files are stored.
///
/// This function constructs the directory path `.config/ncoin/keys/` in the user's
/// home directory. It returns the path as a `String` if successful.
///
/// # Errors
///
/// Returns an error if the home directory cannot be retrieved or converted to a valid
/// string representation.
///
/// # Examples
///
/// ```rust no run
/// # use crate::wallet::key_path;
/// # fn main() {
/// let path = key_path().unwrap_or_else(|err| {
///     panic!("Failed to get wallet path: {}", err);
/// });
/// println!("Wallet files are stored at: {}", path);
/// # }
/// ```
fn key_path() -> Result<String, Box<dyn Error>> {
    let mut path_root: PathBuf = home_dir().ok_or("Value is None")?;
    path_root.push(".config/ncoin/keys/");

    Ok(path_root.to_str().ok_or("Value is None")?.to_string())
}

/// Generates a new pair of cryptographic keys for the wallet.
///
/// This function generates a new pair of secret and public keys using the secp256k1
/// elliptic curve cryptography algorithm. If the wallet files already exist, it retrieves
/// and returns the existing keys instead.
///
/// # Errors
///
/// Returns an error if key generation fails or if there is an issue retrieving existing keys.
///
/// # Examples
///
/// ```rust no run
/// # use crate::wallet::create_key_pair;
/// # fn main() {
/// let (secret_key, public_key) = create_key_pair().unwrap_or_else(|err| {
///     panic!("Failed to generate key pair: {}", err);
/// });
/// println!("Generated new key pair successfully");
/// # }
/// ```
fn create_key_pair(name: &String) -> Result<(SecretKey, PublicKey), Box<dyn Error>> {
    if does_wallet_exist(name)? {
        println!("\nThe keys already exist, getting them now");
        let (decoded_secret_key, decoded_public_key) = get_keys(name)?; 
        return Ok((decoded_secret_key, decoded_public_key));
    }

    let mut rng: OsRng = OsRng;
    let mut buffer: [u8; 32] = [0u8; 32];
    rng.fill_bytes(&mut buffer);

    let secp: Secp256k1<secp256k1::All> = Secp256k1::new();
    let secret_key: SecretKey = SecretKey::from_slice(&buffer)?;
    let public_key: PublicKey = PublicKey::from_secret_key(&secp, &secret_key);

    Ok((secret_key, public_key))
}

/// Checks if the provided filename contains invalid characters.
///
/// This function checks if the provided filename contains any of the following invalid
/// characters: `\ / : * ? " < > |`.
///
/// # Arguments
///
/// * `filename` - A string slice representing the filename to check.
///
/// # Returns
///
/// A `bool` indicating whether the filename contains invalid characters (`true`) or not (`false`).
///
/// # Examples
///
/// ```rust no run
/// use crate::wallet::contains_invalid_chars;
///
/// assert!(contains_invalid_chars("my_wallet")); // false, no invalid characters
/// assert!(contains_invalid_chars("my/wallet")); // true, contains '/'
/// ```
fn contains_invalid_chars(filename: &str) -> bool {
    let invalid_chars: [char; 9] = ['\\', '/', ':', '*', '?', '"', '<', '>', '|'];

    filename.chars().any(|c: char| invalid_chars.contains(&c))
}

/// Stores the generated key pair securely in wallet files.
///
/// This function encodes and stores the provided `KeyPair` into two separate files:
/// `wallet` (for secret key) and `wallet.pub` (for public key). If the wallet files
/// already exist, it displays the existing keys instead of storing new ones.
///
/// # Errors
///
/// Returns an error if it fails to create directories for storing keys or encounters
/// an issue while writing key data to files.
///
/// # Examples
///
/// ```rust no run
/// # use crate::wallet::{store_keys, KeyPair};
/// # fn main() {
/// let keypair = KeyPair {
///     secret_key: vec![1, 2, 3, 4],
///     public_key: vec![5, 6, 7, 8],
/// };
///
/// if let Err(e) = store_keys(&keypair) {
///     println!("Failed to store keys: {}", e);
/// }
/// # }
/// ```
fn store_keys(keypair: &KeyPair, name: &String) -> Result<(), Box<dyn Error>> {
    let encoded_secret_key: String = encode(&keypair.secret_key);
    let encoded_public_key: String = encode(&keypair.public_key);

    let mut exists: bool = false;

    if does_wallet_exist(name)? {
        println!("The keys already exist, showing them now\n");
        exists = true;
    }

    println!("!!! NEVER SHARE THIS EVER !!!\nYour secret key (hex) is: \n{}\n", encoded_secret_key);
    println!("Your public key (hex) is: \n{}\nThis is also your wallet address\n", encoded_public_key);

    if exists { return Ok(()) };

    create_dir_all(key_path()?)?;

    let mut private_key_file: File = File::create(key_path()? + name)?;
    private_key_file.write_all(encoded_secret_key.as_bytes())?;

    let mut public_key_file: File = File::create(key_path()? + name + ".pub")?;
    public_key_file.write_all(encoded_public_key.as_bytes())?;

    Ok(())
}
