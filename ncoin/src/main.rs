/*!
NCoin - A simple take on the blockchain in Rust

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at <http://mozilla.org/MPL/2.0/>.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/

//! NCoin is a simple implementation of a blockchain in Rust. This module
//! provides the main entry point for the application and includes the 
//! necessary imports and setup.

pub mod structs;
pub mod hashing;
pub mod genesis;
pub mod miner;
pub mod transaction;
pub mod visualizer;
pub mod ncoin_cli;
pub mod wallet;
pub mod save_load;

use crate::structs::Block;
use crate::genesis::make_genesis_block;
use crate::ncoin_cli::cli_loop;
use crate::save_load::{save_blockchain, load_blockchain};

/// Main function to initialize/load the blockchain and start the CLI loop.
/// 
/// This function tries to load an existing blockchain and if it cant it 
/// creates the genesis block, initializes the blockchain with it or the 
/// loaded chain, and then starts the command-line interface loop which allows
/// users to interact with the blockchain. When the CLI exits it saves the 
/// blockchain before exiting.
fn main() {
    let mut blockchain: Vec<Block> = vec![];
    if let Some(blockchain_test) = load_blockchain() {
        blockchain = blockchain_test;
        cli_loop(&mut blockchain);
    } else if let Some(genesis_block) = make_genesis_block(&blockchain) {
        blockchain.push(genesis_block);
        cli_loop(&mut blockchain);
    } else {
        println!("\n!!! WARNING !!! The blockchain is missing a genesis block.\n\
        DO NOT attempt to mine a block without a genesis block, as it WILL cause a crash.\n\
        Consider this a safeboot mode.\n\
        Use it only to address any reported errors during genesis block creation,\n\
        then RESTART the application!!!");

        cli_loop(&mut blockchain);
    }

    match save_blockchain(&blockchain) {
        Ok(()) => (),
        Err(_) => println!("Failed to save blockchain")
    }
}