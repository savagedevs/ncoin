/*!
hashing.rs - Implements hashing functions for the NCoin project.

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at <http://mozilla.org/MPL/2.0/>.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/

use sha2::{Sha256, Digest};
use crate::{structs::BlockData, transaction::serialize_transaction};

/// Computes the SHA-256 hash of a block's data.
///
/// This function takes a reference to `BlockData`, concatenates its fields, and computes
/// the SHA-256 hash of the resulting byte array.
///
/// # Arguments
///
/// * `block_data` - A reference to the `BlockData` to hash.
///
/// # Returns
///
/// A `String` representing the SHA-256 hash of the block's data.
///
/// # Examples
///
/// ```rust no run
/// let block_hash = get_block_hash(&block_data);
/// ```
pub fn get_block_hash(block_data: &BlockData) -> String {
    let previous_hash_bytes: &[u8] = block_data.previous_hash.as_bytes();
    let mut transaction_data: Vec<u8> = Vec::new();
    for transaction in &block_data.transactions {
        transaction_data.extend(serialize_transaction(transaction));
    }

    let index_hash: String = get_sha256_hash_string(&block_data.index.to_ne_bytes());
    let unix_timestamp_hash: String = get_sha256_hash_string(&block_data.unix_timestamp.to_ne_bytes());
    let nonce_hash: String = get_sha256_hash_string(&block_data.nonce.to_ne_bytes());
    let previous_hash_hash: String = get_sha256_hash_string(previous_hash_bytes);
    let data_hash: String = get_sha256_hash_string(&transaction_data);

    let hash: String = get_sha256_hash_string((
        index_hash +
        unix_timestamp_hash.as_str() +
        nonce_hash.as_str() +
        previous_hash_hash.as_str() +
        data_hash.as_str())
        .as_bytes());
    
    hash
}

/// Computes the SHA-256 hash of a transaction's data.
///
/// This function takes the various components of a transaction, concatenates their
/// SHA-256 hashes, and computes the SHA-256 hash of the resulting byte array.
///
/// # Arguments
///
/// * `sender_pub_key` - The public key of the sender.
/// * `receiver_pub_key` - The public key of the receiver.
/// * `amount_in_nanos` - The amount of cryptocurrency to be transferred, in nanocoins.
/// * `burn_fee` - The burn fee for the transaction.
/// * `last_block_hash` - The hash of the last block in the blockchain when the transaction was created.
/// * `public_memo` - A public memo for the transaction.
///
/// # Returns
///
/// A `String` representing the SHA-256 hash of the transaction's data.
///
/// # Examples
///
/// ``` rust no run
/// let trans_hash = get_trans_hash(sender_pub_key, receiver_pub_key, amount_in_nanos, burn_fee, last_block_hash, public_memo);
/// ```
pub fn get_trans_hash(sender_pub_key: String, receiver_pub_key: String, amount_in_nanos: u64, burn_fee: u64, last_block_hash: String, public_memo: String) -> String {
    let sender_key_hash: String = get_sha256_hash_string(sender_pub_key.as_bytes());
    let receiver_key_hash: String = get_sha256_hash_string(receiver_pub_key.as_bytes());
    let amount_hash: String = get_sha256_hash_string(&amount_in_nanos.to_ne_bytes());
    let burn_fee_hash: String = get_sha256_hash_string(&burn_fee.to_ne_bytes());
    let previous_hash_hash: String = get_sha256_hash_string(last_block_hash.as_bytes());
    let memo_hash: String = get_sha256_hash_string(public_memo.as_bytes());

    let hash: String = get_sha256_hash_string((
        sender_key_hash +
        receiver_key_hash.as_str() +
        amount_hash.as_str() +
        burn_fee_hash.as_str() +
        previous_hash_hash.as_str() +
        memo_hash.as_str())
        .as_bytes());

    hash
}

/// Computes the SHA-256 hash of the input bytes and returns it as a hexadecimal string.
///
/// # Arguments
///
/// * `input` - A byte slice to hash.
///
/// # Returns
///
/// A `String` representing the hexadecimal SHA-256 hash of the input.
///
/// # Examples
///
/// ```rust no run
/// let hash_string = get_sha256_hash_string(b"example input");
/// ```
fn get_sha256_hash_string(input: &[u8]) -> String {
    let mut hasher = Sha256::new();
    hasher.update(input);

    let hashed_result = hasher.finalize();
    let hex_string: String = format!("{:x}", hashed_result);

    hex_string
}