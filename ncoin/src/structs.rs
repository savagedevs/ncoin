/*!
structs.rs - Defines the top-level structs for the NCoin project.

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at <http://mozilla.org/MPL/2.0/>.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/

use serde::{Serialize, Deserialize};

/// Represents the data contained in a blockchain block.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BlockData {
    /// The public address of the miner
    pub miner_address: String,
    /// The index of the block in the blockchain.
    pub index: u64,
    /// The Unix timestamp of when the block was created.
    pub unix_timestamp: u64,
    /// The nonce used for the proof-of-work algorithm.
    pub nonce: u64,
    /// The hash of the previous block in the blockchain.
    pub previous_hash: String,
    /// The data contained in the block, represented as a vector of byte vectors.
    pub transactions: Vec<Transaction>,
}

/// Represents a block in the blockchain.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Block {
    /// The hash of the block.
    pub block_hash: String,
    /// The data contained in the block.
    pub block_data: BlockData,
}

/// Represents a transaction in the blockchain.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Transaction {
    /// The public key of the sender.
    pub sender_pub_key: String,
    /// The public key of the receiver.
    pub receiver_pub_key: String,
    /// The amount of cryptocurrency to be transferred, in nanocoins.
    pub amount_in_nanos: u64,
    /// The burn fee for the transaction.
    pub burn_fee: u64,
    /// The hash of the last block in the blockchain when the transaction was created.
    pub last_block_hash: String,
    /// A public memo for the transaction.
    pub public_memo: String,
    /// The digital signature of the transaction.
    pub transaction_signature: String,
}

/// Represents a key pair (secret key and public key).
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct KeyPair {
    /// The secret key.
    pub secret_key: Vec<u8>,
    /// The public key.
    pub public_key: Vec<u8>,
}
