## 05/29/2024 - Repo init: 
- Added LICENSE, README.md and, shell.nix
- Initilized a blank rust project

## 05/29/2024 - v0.0.1
- Created basic structs for blocks
- Created a way to get a block hash
- Inital testing with genesis block
- Set the stage for a blockchain

## 05/29/2024 - v0.0.2
- Fixed time error in changelog
- Reduced block data to 4 bytes for testing
- Derived `Debug` and `Clone` on structs
- Made first test blockchain
- Debug printing of the blockchain
- Cleaned up the code

## 05/30/2024 - v0.0.3
- Refactored code into for readability
- Renamed `sha256_hash` to `get_sha256_hash_string`
- Added `genesis.rs`, `hashing.rs`, and `structs.rs`
- Updated the readme to better share the reason for a future feature.

## 05/30/2024 - v0.0.4
- Added `index`, `unix_timestamp`, and `nonce` to the `BlockData` struct
- Updated the genesis block to relect this

## 05/30/2024 - v0.0.5
- Added `miner.rs` to handle block generation
- Removed duplicated code
- Added new block data to the hash
- Cleaned up `main.rs` and `genesis.rs` by making use of the miner
- Changed to start block at index 1 instead of 0
- Changed previous hash of genesis back to `None`

## 05/31/2024 - v0.1.0
- Added `hex` crate to deal with hex conversion
- Cleaned up formatting in `hashing.rs`
- Renamed `mine_block()` to `create_block()`
- Implemented nonce difficulty based mining into `miner.rs`
- Added `mine_block()` and `is_nonce_valid()` valid functions into `miner.rs`
- Cleaned up `create_block()` to only make blocks

## 05/31/2024 - v0.1.1
- Made mining multithreaded
- Changed code to make 5 test blocks
- Added debug output to the genesis block
- Replaced expect block for `byte_index` and `bit_index`

## 06/01/2024 - v0.1.2
- Defined the format for transactions
- Created a struct for the transactions
- Created a `new` and `defualt` implementation in `transaction.rs`
- Set a defualt coin burn rate, will use math to find better value in future

## 06/02/2024 - v0.1.3
- Changed `block_data` to be a `Vec<Vec<u8>>` to make transaction serialization easier
- Threads are now automatically chossen to be half of available resources
- Updated files to reflect change to `block_data`
- Fixed typos relateding to transaction amount

## 06/02/2024 - v0.2.0
- Finilized the setup for transactions
- Created `visualizer.rs` to hold a way to see the blocks better
- Replaced random data with placeholder transactions for blocks
- Changed `genesis.rs` and 'main.rs' to use the `visualize_block()` function instead of dbg!()
- Created a way to serialize and deserialize transactions
- Changed `amount` to `amount_in_nanos` with one nano being 1/1,000,000th of an Ncoin

## 06/03/2024 - v0.2.1
- Added `ncoin_cli.rs` to hold all code related to the cli
- Created basic CLI for the app that allows exiting
- Added a check for a command line argment to act like normal or go to the WIP CLI
- Updated `visualize_block()` to better format the blocks and transactions

## 06/03/2024 - v0.2.2
- Reworked the math for the brun fee and added minimum value of 10 nanos
- Removed unneeded code from `main.rs`
- Moved block creating code to `ncoin_cli.rs`
- Enhanced the functionality of the CLI
- The CLI can now make transactions, mine blocks, and inspect the blockchain
- Fixed the file header for `ncoin_cli.rs` to add missing word

## 06/07/2024 - v0.3.0
- Added two new crates, `secp256k1` and `home`
- Added wallet.rs to handle wallet creation, storage, and ballance calculation (WIP)
- Added new option to the CLI to create/manage a wallet
- Added a new `KeyPair` struct to `structs.rs` to hold a key pair
- Put in a valid wallet address to the genesis block
- Created `wallet_setup()`, `get_keys()`, `create_key_pair()`, and `store_keys()` in wallet.rs

## 06/07/2024 - v0.3.1
- Moved `make_transaction()` to transaction.rs
- Cleaned up `ncoin_cli.rs`

## 06/08/2024 - v0.3.2
- Fixed bug in `visualize_block()` where it would show the same value for a transactions public and private key
- Added `does_wallet_exist()` public function to test for if a wallet exists when needed
- Renamed `make_transaction()` to `create_transaction()`
- Updated `create_transaction()` to use wallets and sign transactions
- Now `create_transaction()` will propogate errors to the caller
- Added `get_trans_hash()` function to `hashing.rs` to get a digest for transaction signing
- Calculating burn rate is now a part of making a transaction
- Updated the CLI to make use of the better transaction system

## 06/08/2024 - v0.3.3
- Changed check for wallet validity to not cause a crash
- Updated/added comments for Rustdoc documentation

## 06/09/2024 - v0.3.4
- Changed `BlockData` to include a place for the miner address, for block rewards
- Removed the transaction in the genesis block
- Added warnings on startup for a non exisant wallet because genesis cannot be made without one
- Added a "safeboot" mode to the app if a wallet does not exist
- Updated the visualizer to show the miner address
- Updated `create_block()` to return a propagated `Option` if it proves imposible to mine a block

## 06/10/2024 - v0.3.5
- Added `is_transaction_valid()` to test for transaction validity
- The miner will now check if a transaction is valid before adding to the block to be mined

## 06/11/2024 - v0.4.0
- Updated rustdoc comments for better formatting
- Added `get_wallet_ballance()` function to `wallet.rs` to get the wallet ballance in nanos
- Updated `create_transaction()` to check for transaction validity before creating it
- Updated the miner to need a refrence to the blockchain for transaction validity checking
- Updated the CLI to let you see your balance
- Added a `view_ballance()` fucntion to `wallet.rs` for the CLI to use

## 06/12/2024 - v0.5.0
- Added multi wallet support
- Updated both `mine_block()` and `create_transaction()` to ask for a wallet
- Updated the ballance checking system in `ncoin_cli.rs` to ask for a wallet
- Added the `get_wallet_name()` function to `wallet.rs` to get the name of a wallet
- Updated all wallet functions to support multiple wallets
- Changed the testing `FEE_PERCENT` to 0.5% of the transaction and the minimum burn fee to 500 nanos.

## 06/13/2024 - v0.5.1
- Added `save_load.rs` to hold saving and loading functions
- Added `save_blockchain()`, `load_blockchain()`, and `save_single_block()` functions
- Curently the save system will just rpint the blocks to the terminal before closing
- Currently the load system will just return an empty vector

## 06/14/2024 - v0.5.2
- The `BlockData` now holds `transactions` instead of `block_data` and is of type `Vec<Transaction>` now
- The blockchain is now made to hold only transactions to prevent bugs
- All needed functions have been updated to return or use `Vec<Transaction>` instead of `Vec<u8>`
- Removed the `deserialize_transaction()` function as it is was no longer needed

## 06/15/2024 - v0.5.3
- Blockchain saving logic is completed
- Removed `save_single_block()` and rolled it into `save_blockchain()` directly
- Now `save_blockchain()` will propagate any error in saving to the caller
- Updated `main.rs` to deal with potential errors from `save_blockchain()`

## 06/16/2024 - v0.6.0
- Blockchain loading logic is completed
- Added function `load_block()` to help `load_blockchain()` load each block form disk
- Updated `main.rs` to check for an existing blockchain first
- Updated `load_blockchain()` to return an `Option<Block>` to tell a chain does or does not exist
