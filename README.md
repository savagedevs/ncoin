# NCoin

NCoin is a decentralized cryptocurrency implemented in Rust, designed to provide a clean blockchain architecture with adaptive difficulty, constant block rewards, and support for arbitrary data within blocks.

## Features

- **Blockchain Consensus**: Utilizes a longest chain consensus mechanism with sha256 hashing.
- **Dynamic Difficulty**: Automated difficulty adjustment based on leading zeros in sha256 hashes for consistent block creation (approximately every 5 minutes).
- **Block Metadata**: Each block includes Index, Unix timestamp, block hash, previous block hash, and the block data.
- **Arbitrary Data**: Blocks support arbitrary data amounts, allowing for diverse applications beyond traditional transactions.
- **Economic Model**: Implements a constant block reward structure to incentivize miners and maintain network security.
  
## Getting Started

To get a copy of NCoin up and running on your local machine for development and testing purposes, follow these steps:

### Prerequisites

- Access to the Nix Package Manager

### Installation

1. Clone the repository:
   ```bash
   git clone https://github.com/yourusername/NCoin.git
   cd NCoin
   ```

2. Build the project:
   ```bash
   cargo build
   ```

### Usage

To start a NCoin node and join the network: (WIP)

```bash
cargo run
```

## Contributing

Thank you for considering contributing to NCoin! To contribute, please follow these guidelines:

1. Fork the repository and create your branch from `master`.
2. Make your changes, ensuring the code style and tests are updated.
3. Submit a pull request detailing the changes made and any relevant information.

## License

This project is licensed under the MPL v2.0 License - see the [LICENSE](LICENSE) file for details.

## Acknowledgments

- Inspiration from Bitcoin and other blockchain implementations.
- Rust community for robust tooling and support.

## Contact

For questions or suggestions regarding NCoin, feel free to contact the maintainers:

- Email: daniel@savagedevs.com
